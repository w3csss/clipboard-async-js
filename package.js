// Package metadata for Meteor.js.

Package.describe({
  name: 'zenorocha:clipboard-async',
  summary: 'Modern copy to clipboard. No Flash. Just 3kb.',
  version: '1.0.1',
  git: 'https://gitee.com/w3csss/clipboard-async-js',
});

Package.onUse(function (api) {
  api.addFiles('dist/clipboard.js', 'client');
});
